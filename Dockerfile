FROM base/archlinux:latest

ARG username=kelvin
ARG password=123

RUN pacman -Syu --noconfirm --needed base base-devel clang cmake git openssh neovim python-neovim &&\
    useradd -m ${username} &&\
    echo "${username}:${password}" | chpasswd &&\
    echo "${username} ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers &&\
    echo "Defaults env_keep += \"http_proxy https_proxy ftp_proxy socks_proxy\"" >> /etc/sudoers

COPY nvim /home/${username}/.config/nvim/
COPY ycm_extra_conf.patch /home/${username}/

RUN chown -R ${username}:${username} /home/${username}/.config &&\
    chown ${username}:${username} /home/${username}/ycm_extra_conf.patch

USER ${username}
WORKDIR /home/${username}

RUN curl -LO https://aur.archlinux.org/cgit/aur.git/snapshot/package-query.tar.gz &&\
    tar -xf package-query.tar.gz &&\
    cd package-query &&\
    makepkg -sri --noconfirm &&\
    cd .. &&\
    curl -LO https://aur.archlinux.org/cgit/aur.git/snapshot/yaourt.tar.gz &&\
    tar -xf yaourt.tar.gz &&\
    cd yaourt &&\
    makepkg -sri --noconfirm &&\
    cd .. &&\
    rm -rf yaourt* package-query* &&\
    curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

RUN yaourt -S --noconfirm bear cquery &&\
    nvim --headless +PlugInstall +qa &&\
    cp ~/.local/share/nvim/plugged/YouCompleteMe/third_party/ycmd/examples/.ycm_extra_conf.py ~ &&\
    patch -p0 < ycm_extra_conf.patch &&\
    rm ycm_extra_conf.patch
