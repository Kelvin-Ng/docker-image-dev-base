set number
set smartindent
set softtabstop=4 shiftwidth=4 expandtab
set encoding=utf-8
set mouse=a

autocmd FileType tex setlocal indentexpr=

nnoremap ;k :bp<CR>
nnoremap ;j :bn<CR>
nnoremap ;l :e#<CR>

set guicursor=
