" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'

let hostname = substitute(system('hostname'), '\n', '', '')

call plug#begin('~/.local/share/nvim/plugged')

Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'iCyMind/NeoSolarized'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'rhysd/vim-clang-format'
Plug 'Valloric/YouCompleteMe', { 'do': './install.py --clang-completer --system-libclang' }
Plug 'vim-latex/vim-latex'
Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }

" Initialize plugin system
call plug#end()

" vim-latex
"let g:tex_flavor='xelatex'
let g:tex_flavor='latex'
let g:Tex_ViewRule_pdf='okular'
let g:Tex_DefaultTargetFormat='pdf'
"let g:Tex_ViewRule_dvi='okular'
"let g:Tex_CompileRule_dvi='latex --src-specials -interaction=nonstopmode $*'
"let g:Tex_CompileRule_pdf='xelatex -synctex=1 --interaction=nonstopmode $*'
let g:Tex_CompileRule_pdf='pdflatex -synctex=1 --interaction=nonstopmode $*'
let Tex_FoldedSections=""
let Tex_FoldedEnvironments=""
let Tex_FoldedMisc=""

" vim-airline

let g:airline#extensions#tabline#enabled = 1

" vim-geeknote
let g:GeeknoteFormat="markdown"

source ~/.config/nvim/ycm.vim
source ~/.config/nvim/language_client.vim
