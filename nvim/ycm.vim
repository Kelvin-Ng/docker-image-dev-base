let g:ycm_global_ycm_extra_conf = '~/.ycm_extra_conf.py'

nnoremap <F5> :YcmDiags<CR>
nnoremap <leader>} :YcmCompleter GoToDefinitionElseDeclaration<CR>
nnoremap <leader>[ :YcmCompleter GoToDefinition<CR>
nnoremap <leader>] :YcmCompleter GoToDeclaration<CR>
let g:ycm_global_ycm_extra_conf = '~/.ycm_extra_conf.py'
let g:ycm_confirm_extra_conf = 0
set completeopt-=preview
